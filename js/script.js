// 1. Сделайте функцию, которая отнимает от первого числа второе и делит на
// третье. Числа передаются параметром.

const f1 = (num1, num2, num3) => (num1 - num2) / num3;

const result = f1(6, 2, 1);
console.log (result);


// 2. Сделайте функцию, которая возвращает куб числа и его квадрат. Число
// передается параметром.

const f2 = function(num) {
    return `Квадрат ${num*num}, куб ${num**3}`;
}

console.log (f2(2));


// 3. Напишите функции min и max, которые возвращают меньшее и большее из
// чисел a и b.

let max = (a, b) => {
    if (a > b) {
        return a;
    }
    if (b > a) {
        return b;
    }
    return 'Числа равны'
}

let min = (a, b) => {
    if (a < b) {
        return a;
    }
    if (b < a) {
        return b;
    }
    return 'Числа равны'
}

console.log (`max ${max(1,6)}, min ${min(1,6)}`);


// 4. Напишите две функции: первая ф-ция должна возвращать массив с
// числовыми значениями, диапазон которых должен вводиться пользователем
// с клавиатуры; вторая – выводить полученный массив.


// const arrPush = () => {
//     let arr = [];
//     let numFirst = +prompt ('Введите первое число');
//     let arrLength = +prompt ('Введите второе число');
//     for (i=numFirst; i<=arrLength; i++) {
//         arr.push (i);
//     }
//     return arr;
// }


                               //console.log (arrPush());

// let arrayPrint = arrPush();

// const arrResult = (array) => {
//     console.log (array[j]);
//     j++;
//     if (j<array.length) arrResult(array);
// }

// let j = 0;
// arrResult (arrayPrint);


// 5. Сделайте функцию isEven() (even - это четный), которая параметром
// принимает целое число и проверяет: четное оно или нет. Если четное - пусть
// функция возвращает true, если нечетное — false.


const isEven = (number5) => {
    if (number5 % 2 === 0 ) {
        return true;
    } else {
        return false;
    }
}

console.log (isEven(10));


// 6. Напишите ф-цию, в которую передается массив с целыми числами.
// Верните новый массив, где останутся лежать только четные из этих чисел.
// Для этого используйте вспомогательную функцию isEven из предыдущей
// задачи.

const arr6 = [1, 5, 2, 7, 9, 8, 14, 21];
const f6 = () => {
    const arrNew = [];
    for (let i=0; i<arr6.length; i++) {
        if (isEven(arr6[i]) === true) {
            arrNew.push(arr6[i]);
        }
    }
    return arrNew;
}

console.log (f6());


// 7

// let pyramid = (str, simv) => {
//     for (let i = 1; i <= str; i++) {
//         let n = '';
//         for (let j = 0; j < i; j++) {
//             if (simv == undefined || simv == '') {
//                 document.write(i); 
//             }else {
//                 n += simv;
//             }
//         }
//         document.write(n); 
//         document.write('<br/>');
//     }
// }

// pyramid(prompt('Введите число строк'), prompt('Введите символ'));


//8 Напишите ф-цию, которая рисует равнобедренный треугольник из
//звездочек


const pyramid = (n) => {
    let result = "";
    for(let i = 1 ; i <= n ; i++) {
        for(let j = n ; j > i ; j--) {
          result += " ";
        }
        for(j = 1 ; j<= i ; j++) {
          result += '*' + " ";
        }
        result += "\n";
    }
    return result;
}
console.log(pyramid(5));

const newPyramid = (m) => {
    let result = "";
    for(let i = 1 ; i <= m ; i++) {
        for(let j = 1 ; j < i ; j++) {
          result += " ";
        }
        for(j = m ; j >= i ; j--) {
          result += '*' + " ";
        }
        result += "\n";
    }
    return result;
}
console.log(newPyramid(5));


// 9. Напишите ф-цию, которая возвращает массив заполненный числами
// Фибоначи от 0 до 1000.

const fibArray = () => {
    const arr9 = [];
    for (let i = 0; ;i++) {
        if (i === 0 || i === 1) {
            arr9[i] = 1;
        } else { 
            arr9[i] = arr9[i-1] + arr9[i-2];
        }
        if (arr9[i] > 1000) break;
    }
    return arr9;
}

console.log (fibArray());


// 10. Дано число. Сложите его цифры. Если сумма получилась более 9-ти,
// опять сложите его цифры. И так, пока сумма не станет однозначным числом
// (9 и менее). Исп. Рекурсию.


const summa = (num) => {
    let sum = 0;
    for (var i = 0; i < num.length; i++) {
        sum += +num[i];
    }
    if (sum > 9) {
        return summa(sum + '');
    } else {
        return sum;
    }
}

let num = '28769';
let resultNum = summa(num);
console.log('Конечное число = ' + resultNum);


// 11. Дан массив с числами (передается параметром). Выведите
// последовательно его элементы, используя рекурсию и не используя цикл.

const printArray = (arr) => {
    console.log (arr[i]);
    i++;
    if (i < arr.length) {
        printArray(arr);
    }
}

let i = 0;
printArray ([1, 67, 87, 235, 'кот', 81873]);


// 12. Напишите ф-цию, запрашивающую имя, фамилия, отчество и номер
// группы студента и выводящую введённые данные в следующем виде

let strFirst = '* Домашняя работа: «Функции» *';
let grp = prompt ('Введите номер группы');
let surname = prompt ('Введите вашу фамилию');
let nameF = prompt ('Введите ваше имя');
let patronymic = prompt ('Введите ваше отчество');
let strSecond = `* Выполнил: студент гр. ${grp} *`;
let strThird = `* ${surname} ${nameF} ${patronymic} *`;


let length = 0;
if (strFirst.length > strSecond.length && strFirst.length > strThird.length) {
    length = strFirst.length - 1;
}

if (strSecond.length > strFirst.length && strSecond.length > strThird.length) {
    length = strSecond.length - 1;
}

if (strThird.length > strFirst.length && strThird.length > strSecond.length) {
    length = strThird.length - 1;
}

let drawRamka = (length) => {
    let ramka = '';
    for (let i = 0; i <= length; i++){
        ramka += '*';
    }
    console.log(ramka);
}

drawRamka(length);
console.log (strFirst);
console.log (strSecond);
console.log (strThird);
drawRamka(length);




